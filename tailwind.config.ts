import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    'node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}',   
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
      },
      fill: {
        yellow: '#FCDC4D', // Ajout d'une couleur jaune personnalisée
        gray: '#9CA3AF',  // Couleur grise pour l'état inactif
      },
    },
  },
  plugins: [
    require('flowbite/plugin'),
  ],
};
export default config;