import axios from "axios";
import { TvShows } from "@/entities/TvShows";

export async function fetchAlltvShows() {
    const response = await axios.get<TvShows[]>('/api/show');
    return response.data;
}

export async function fetchOnetvShow(id: number) {
    const response = await axios.get<TvShows>('/api/show/' + id);
    return response.data;
}

export async function deleteTvShow(TvShow: TvShows) {
    const response = await axios.delete('api/show/' + TvShow.id);
    return response.data
}

export async function fetchSearch(search: string) {
    const response = await axios.get<TvShows[]>('/api/show/search/' + search);
    return response.data;
}