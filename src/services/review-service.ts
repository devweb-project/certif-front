import { Reviews } from "@/entities/Reviews";
import axios from "axios";

export async function fetchAllReviews() {
    const response = await axios.get<Reviews[]>('/api/review');
    return response.data;
}
// export async function calculateAverageRating(reviews: Reviews[]) {
//     if (reviews.length === 0) {
//         return 0;
//     }
//     const totalRating = reviews.reduce((sum, review) => sum + (review.rating || 0), 0);
//     const averageRating = totalRating / reviews.length;
//     return averageRating;
// }

export async function fetchOneReview(id: number) {
    const response = await axios.get<Reviews>('/api/review/' + id);
    return response.data;
}

export async function postReview(Review: Reviews, showId: number) {
    console.log("Review sent to POST method:", Review);

    try {
        const response = await axios.post<Reviews>('/api/review/' + showId, Review);
        console.log("Réponse reçue après POST:", response.data);
        
        return response.data;
    } catch (error) {
        console.error("Erreur lors de l'envoi de la requête POST:", error);
        throw error; 
    }
}

export async function updateReview(Review: Reviews) {
    const response = await axios.put<Reviews>('/api/review/' + Review.id, Review);
    return response.data;
}

export async function deleteReview(reviewId: number) {
    await axios.delete(`/api/review/${reviewId}`);
}

