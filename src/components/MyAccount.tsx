import { fetchUser, fetchWatchList } from "@/auth/auth-service";
import { TvShows } from "@/entities/TvShows";
import { User } from "@/entities/User";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { deleteShowsOfWatchList } from "@/auth/auth-service";
import { watch } from "fs";



export default function MyAccount() {
    const router = useRouter();
    const { id } = router.query;
    const [watchList, setWatchList] = useState<TvShows[]>([]);
    const [user, setUser] = useState<User>();

    useEffect(() => {
        fetchUser()
            .then((data) => {
                setUser(data);
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    router.push("/login");
                }
            });
    }, [id]);

    useEffect(() => {
        if (user) {
            fetchWatchList()
                .then((data) => {
                    setWatchList(data);
                })
                .catch((error) => {
                    console.log(error);
                    return {
                        notFound: true
                    }
                });
        }
    }, [user]);
    
    const handleDeleteFromWatchList = (showId: any) => {
        deleteShowsOfWatchList(showId)
        .then(() => {
            setWatchList(prevWatchList => prevWatchList.filter(show => show.id !== showId));
        })
        .catch(error => {
            console.error("Erreur lors de la suppression", error);
        });
    };


    return (
        <div className="bg-[#0E3B43]">
            <h1 className="text-[#FCDC4D] text-xl text-center font-bold">
                Bienvenue dans ton espace {user?.email}
            </h1>
            <div className="max-w-sm py-4 px-2 flex justify-center items-center mx-auto">
            <p className="text-white text-medium text-center">Tu peux mettre à jour ta liste, regarder la série de ton choix en streaming et donner ton avis avec une note et un commentaire pour aider la communauté à faire le bon choix!</p>
            </div>
            <div className="flex justify-center items-center">

                    <Link href={"/"} className="my-8 text-white text-center font-bold hover:text-[#FCDC4D] transition duration-500">
                        <svg color="white" className="w-6 h-6 inline-block align-bottom" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" stroke="currentColor" viewBox="0 0 24 24">
                            <path d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
                        </svg>
                        Retour à l'Accueil
                        <i className="fas fa-chevron-circle-left fa-fw"></i>
                    </Link>

            </div>
            <div className="bg-[#0E3B43] border-[#86BBBD] text-white font-bold text-2xl p-2">
                <p>Ma WatchList</p>
                <div className="showList card-container container-fluid bg-[#86BBBD] overflow-x-auto">
                    {watchList.length === 0 ? (
                        <p className="text-white h-24">Votre liste est vide.</p>
                    ) : (
                        watchList.map((show) => (
                            <div
                                key={show.id}
                                className="bg-[#0E3B43] border-[#86BBBD] card-container"
                            >
                                <div className="p-3 w-[300px] flex flex-col justify-center items-center min-h-full">
                                    <Link className="showPage" href={"/show/" + show.id}>
                                        <img
                                            src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/${show?.image}`}
                                            alt="tvshow image"
                                            className="rounded-lg card-img-top justify-center mb-4 imageShow"
                                        />
                                        <p className="text-white text-center">{show.name}</p>
                                    </Link>

                                    <div className="space-x-2">
                                        {show?.link && (
                                            <div className="flex justify-center items-center">
                                                <a target='_blank' href={show.link} className="flex font-bold text-[#0E3B43] items-center px-1 hover:text-[#86BBBD]">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" className="bi bi-play-circle text-2xl font-bold watch" viewBox="0 0 16 16">
                                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                        <path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z" />
                                                    </svg>
                                                    <span className="text-white">Regarder</span>
                                                </a>
                                            </div>
                                        )}
                                        <button
                                            onClick={() => handleDeleteFromWatchList(show.id!)} className="flex font-bold text-[#0E3B43] items-center px-1 hover:text-[#86BBBD]"
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26"
                                                height="26"
                                                fill="#EE0202"
                                                className="bi bi-x-circle text-2xl font-bold watch"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                            </svg>
                                            <span className="text-white">Supprimer</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))
                    )}
                </div>
            </div>
        </div>
    );
}
