
import { Footer } from 'flowbite-react';

import { BsFacebook, BsInstagram, BsTwitter, BsTiktok } from 'react-icons/bs';

export default function FooterWithLogo() {
  return (
    <Footer container className='bg-[#86BBBD] footer rounded-none'>
      <div className="w-full text-center">
        <h2 className='flex mb-4 justify-center text-white text-3xl font-semibold'> Suivez-nous sur les réseaux</h2>
        <div className="mt-4 flex my-4 space-x-6 sm:mt-0 justify-center">
          <Footer.Icon
            className='text-white'
            href="#"
            icon={BsFacebook}
          />
          <Footer.Icon
            className='text-white'
            href="#"
            icon={BsInstagram}
          />
          <Footer.Icon
            className='text-white'
            href="#"
            icon={BsTwitter}
          />
          <Footer.Icon
            className='text-white'
            href="#"
            icon={BsTiktok}
          />
        </div>
        <Footer.Divider />
        <div className="w-full my-4 flex flex-col items-center md:flex-row sm:justify-between text-center">
          <span className="self-center text-[#0E3B43] text-3xl font-semibold whitespace-nowrap dark:text-[#0E3B43]">WhatshoW</span>
          <Footer.LinkGroup className="mt-4 sm:mt-0 flex flex-col sm:flex-row">
            <Footer.Link href="http://localhost:3000/" className='font-semibold text-white text-center sm:text-left sm:mr-4'>
              Accueil
            </Footer.Link>
            <span className="self-center text-white mx-4 hidden md:block">|</span>
            <Footer.Link href="#" className='font-semibold text-white text-center sm:text-left sm:mr-4'>
              Politique de confidentialité
            </Footer.Link>
            <span className="self-center text-white mx-4 hidden md:block">|</span>
            <Footer.Link href="#" className='font-semibold text-white text-center sm:text-left sm:mr-4'>
              Mentions Légales
            </Footer.Link>
            <span className="self-center text-white mx-4 hidden md:block">|</span>
            <Footer.Link href="#" className='font-semibold text-white text-center sm:text-left'>
              Contact
            </Footer.Link>
          </Footer.LinkGroup>
        </div>

      </div>
    </Footer>
  )
}
