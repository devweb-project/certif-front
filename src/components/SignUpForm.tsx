import { AuthContext } from "@/auth/auth-context";
import { login, postUser } from "@/auth/auth-service";
import { User } from "@/entities/User";
import { Button } from "flowbite-react";
import Link from "next/link";
import { useRouter } from "next/router";
import { FormEvent, useContext, useState } from "react";



export default function SignUpForm() {
    const router = useRouter();
    const { token, setToken } = useContext(AuthContext);
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [error, setError] = useState('');
    const [user, setUser] = useState<User>({
        email: '',
        password: '',
    });

    function handleChangeLogin(event: any) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmitLogin(event: FormEvent) {
        event.preventDefault();

        try {
            const data = await postUser(user);
            setToken(data.token);
            setFormSubmitted(true);
        } catch (error: any) {
            if (error.response?.status == 400) {
                setError('Une erreur est survenue');;
            }
            else {
                setError('Server error');
            }
        }
    }

    return (
        <>
            <div className="w-full pt-2 p-4">
                <div className="mx-auto md:p-6 md:w-1/2">
                    <div className="flex flex-wrap justify-between">
                        <h1 id="inscription" className="text-2xl text-[#86BBBD] hover:text-[#86BBBD] transition duration-500 p-4">
                            <i className="fas fa-sign-in-alt fa-fw fa-lg"></i>
                            S'inscrire
                        </h1>
                    </div>

                    <div className="bg-[#0E3B43] shadow-md rounded px-8 pt-6 pb-8 mb-4">
                        {formSubmitted ? (<p className="text-white text-xl">Inscription réussi, connecte toi!</p>
                        ) : (
                            <form onSubmit={handleSubmitLogin} method="POST" action="#login">
                                {error && <p className="error-msg">{error}</p>}
                                <div className="mb-8">
                                    <label htmlFor="email" className="block text-white text-sm font-bold mb-2">
                                        <span className="text-red-500">&nbsp;*</span>
                                        Email
                                    </label>
                                    <div className="mt-1 relative rounded-md shadow-sm">
                                        <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                            <svg className="h-5 w-5 text-[#86BBBD]" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path></svg>
                                        </div>
                                        <input
                                            id="email"
                                            className="block pr-10 shadow appearance-none border-2 border-[#86BBBD] rounded-lg w-full py-2 px-4 text-[#86BBBD] mb-3 leading-tight focus:outline-none focus:bg-white focus:border-[#86BBBD] transition duration-500 ease-in-out"
                                            placeholder="votre adresse mail"
                                            type="email"
                                            name="email"
                                            value={user.email}
                                            onChange={handleChangeLogin}
                                            required
                                        />
                                    </div>
                                    <strong className="text-red-500 text-xs italic">Email obligatoire</strong>
                                </div>

                                <div className="mb-8">
                                    <label htmlFor="password" className="block text-white text-sm font-bold mb-2">
                                        <span className="text-red-500">&nbsp;*</span>
                                        Mot de passe
                                    </label>
                                    <div className="mt-1 relative rounded-md shadow-sm">
                                        <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                                            <svg className="h-5 w-5 text-[#86BBBD]" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path></svg>
                                        </div>
                                        <input
                                            name="password"
                                            id="signuppassword"
                                            type="password"
                                            className="block pr-10 shadow appearance-none border-2 border-[#86BBBD] rounded-lg w-full py-2 px-4 text-[#86BBBD] mb-3 leading-tight focus:outline-none focus:bg-white focus:border-[#86BBBD] transition duration-500 ease-in-out"
                                            onChange={handleChangeLogin}
                                            required
                                        />
                                    </div>
                                    <strong className="text-red-500 text-xs italic">Mot de passe obligatoire</strong>
                                </div>

                                <div className="flex justify-center text-center mt-6 mb-6">
                                    <Button className="transition duration-500 bg-[#FCDC4D] hover:bg-red-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                                        Inscription
                                    </Button>
                                </div>
                                <hr />
                            </form>
                        )}
                    </div>
                </div>
            </div>

        </>)
}