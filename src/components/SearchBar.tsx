import { useContext } from "react";
import { AuthContext } from "@/auth/auth-context";
import { useRouter } from "next/router";
import { useState } from "react";
import Link from "next/link";

export default function SearchBar(fieldHome: any) {
    const router = useRouter();
    const { token } = useContext(AuthContext); // Access token from context
    const [searchQuery, setSearchQuery] = useState("");

    function isHome(fieldHome: string) {
        return fieldHome === '/';
    }


    const handleSearch = (event: any) => {
        event.preventDefault();
        const isEmptyOrSpaces = /^\s*$/.test(searchQuery);
        if (!isEmptyOrSpaces) {
            router.push('/search/' + searchQuery);
            setSearchQuery("");
        }
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchQuery(event.target.value);
    };

    return (
        <>
            <div className="searchBar h-60 bg-[#86BBBD] flex justify-center items-center">
                <form role="search" className="sm:max-w-xs lg:max-w-md" onSubmit={handleSearch}>
                    <label htmlFor="default-search" className="mb-2 text-sm font-medium text-[#0E3B43] sr-only dark:text-white">Recherche ta série</label>
                    <div className="relative">
                        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg className="w-4 h-4 text-[#0E3B43] dark:text-[#0E3B43]" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                            </svg>
                        </div>
                        <input
                            type="search"
                            value={searchQuery}
                            onChange={handleChange}
                            name="search"
                            id="search"
                            className="block w-full p-4 pl-10 text-md text-[#0E3B43] border border-gray-300 rounded-3xl bg-gray-50 focus:ring-[#0E3B43] focus:border-[#0E3B43] dark:bg-[#0E3B43] dark:border-[#0E3B43] dark:placeholder-[#0E3B43] dark:text-white dark:focus:ring-gray-500 dark:focus:border-gray-500"
                            placeholder="Recherche ta série ici"
                            aria-label="search"
                        />
                    </div>
                </form>
            </div>
        </>
    );
}
