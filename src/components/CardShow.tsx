import { TvShows } from "@/entities/TvShows";
import Link from "next/link";

interface Props {
    tvShow: TvShows;
}

export default function CarTvShow({ tvShow }: Props) {
    return (


        <div key={tvShow.id} className='bg-[#0E3B43] border-[#86BBBD] card-container'>
            <Link className="showPage" href={"/show/" + tvShow.id}>
                <div className="p-3 w-[300px] flex flex-col justify-center items-center">
                    <img className="rounded-lg card-img-top justify-center mb-4 imageShow" src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/${tvShow.image}`} alt="tvShow image" />
                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-white dark:text-white text-center">{tvShow.name}</h5>
                </div>
            </Link>
        </div>

    );
}

