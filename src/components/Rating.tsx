// import { Reviews } from "@/entities/Reviews";
// import { calculateAverageRating } from "@/services/review-service";
// import router from "next/router";
// import { useEffect, useState } from "react";

// interface RatingProps {
//     setRating: (rating: number) => void; // Prop pour mettre à jour la note
// }

// export default function Rating({ setRating }: RatingProps) {
//     const [hover, setHover] = useState<number | null>(null);
//     const [rate, setRate] = useState<Reviews>();
//     const [averageRating, setAverageRating] = useState<number>(0);
    
//     // useEffect(() => {
//     //     if (rate) {
//     //         calculateAverageRating([rate])
//     //             .then(average => setAverageRating(average))
//     //             .catch(error => {
//     //                 if (error.response.status == 404) {
//     //                     router.push('/404');
//     //                 }
//     //             });
//     //     }
//     // }, [rate]);

//     return (
//         <>
//             <div className="flex justify-center text-center">
//                 {[...Array(5)].map((_, index) => {
//                     const currentRating = index + 1;
//                     const isActive = currentRating <= (hover !== null ? hover : 0);

//                     return (
//                         <div key={index}>
//                             <label>
//                                 <input
//                                     type="radio"
//                                     name="rating"
//                                     value={currentRating}
//                                     onClick={() => setRating(currentRating)}
//                                     onMouseEnter={() => setHover(currentRating)}
//                                     onMouseLeave={() => setHover(null)}
//                                 />
//                                 <svg
//                                     className={`popCorn ${isActive ? 'active-icon' : 'inactive-icon'}`}
//                                     version="1.1"
//                                     id="Capa_1"
//                                     xmlns="http://www.w3.org/2000/svg"
//                                     width="40px"
//                                     height="40px"
//                                     viewBox="0 0 792 792"
//                                 >
//                                     <g>
//                                         <g>
//                                             <path d="M669.584,248.165c0.887-5.423,1.355-10.96,1.355-16.574c0-38.039-21.123-72.163-53.447-89.497
// 			c-8.236-49.393-51.28-87.178-102.979-87.178c-4.612,0-9.199,0.304-13.723,0.899c-18.373-16.193-42.31-25.469-67.221-25.469
// 			c-1.749,0-3.497,0.051-5.246,0.139C404.769,10.72,375.371,0,343.972,0c-54.018,0-101.509,32.134-121.77,81.159
// 			c-16.333,11.531-28.586,27.864-35.125,46.63c-39.661,18.5-66.03,58.807-66.03,103.802c0,5.588,0.405,11.125,1.204,16.574h-18.614
// 			L212.369,792h367.262l108.731-543.835H669.584z M607.254,434.191H490.527c11.023,24.797,17.423,54.22,17.423,85.796
// 			c0,31.108-6.209,60.125-16.941,84.694h83.883l-15.61,78.055H232.719l-15.599-78.055h83.883
// 			c-10.732-24.569-16.941-53.599-16.941-84.694c0-31.563,6.399-60.986,17.423-85.796h-116.74l-15.611-78.055h453.717
// 			L607.254,434.191z M621.42,248.165H170.073c-1.356-5.385-2.065-10.935-2.065-16.574c0-30.449,20.413-57.249,49.633-65.155
// 			l10.15-2.75l1.292-10.441c1.989-15.953,11.708-29.878,25.989-37.253l5.841-3.016l1.938-6.272
// 			c11.024-35.733,43.627-59.745,81.108-59.745c23.898,0,45.997,9.643,62.215,27.154l6.171,6.665l8.857-2.065
// 			c4.029-0.938,8.198-1.419,12.367-1.419c17.334,0,33.781,8.337,43.994,22.301l6.881,9.402l10.985-3.89
// 			c6.069-2.154,12.481-3.244,19.07-3.244c31.678,0,57.438,25.773,57.438,57.438c0,1.14-0.038,2.306-0.102,3.434l-0.785,12.835
// 			l12.442,3.257c23.835,6.247,40.485,27.927,40.485,52.737C623.992,237.319,623.093,242.881,621.42,248.165z"/>
//                                             <path d="M422.179,137.685c-2.116,1.863-3.853,3.992-5.233,6.272c9.884-4.384,21.604-2.344,28.865,5.892
// 			c7.248,8.224,7.806,20.122,2.218,29.372c2.433-1.077,4.764-2.534,6.88-4.397c10.694-9.427,12.051-25.393,3.003-35.644
// 			C448.877,128.917,432.873,128.258,422.179,137.685z"/>
//                                             <path d="M353.108,165.08c-2.445-1.064-5.094-1.787-7.894-2.078c-14.192-1.47-26.837,8.363-28.244,21.959
// 			c-1.406,13.596,8.946,25.811,23.138,27.281c2.813,0.292,5.55,0.127,8.16-0.405c-9.921-4.295-16.371-14.306-15.23-25.216
// 			C334.152,175.711,342.515,167.247,353.108,165.08z"/>
//                                         </g>
//                                     </g>
//                                 </svg>
//                                 {isActive && <span>{currentRating}</span>}                            </label>
//                         </div>
//                     );
//                 })}
//             </div>
//         </>
//     )
// }


