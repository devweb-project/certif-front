import { Button } from "flowbite-react";
import Link from "next/link";

export default function Benefits() {
  return (
    <>
      <div className="bg-[#0E3B43] flex justify-center">
        <div className="bg-[#0E3B43] max-w-sm w-auto my-4 lg:border-2 rounded-lg border-white">
          <h1 className="py-3 text-white text-xl text-center font-bold mt-2">Avantages Inscription</h1>

          <ul className="flex flex-col mt-3 p-2">
            <li className="flex mb-2">
              <img
                src="/television.svg" 
                alt="Créer ta WatchList"
                className="w-6 h-6 fill-[#86BBBD]"
              />
              <p className="py-1 ml-3 text-white text-center font-bold">Créer ta WatchList</p>
            </li>
            <li className="flex mb-2">
              <img
                src="/television.svg"
                alt="Évaluer ce que tu regardes"
                className="w-6 h-6 fill-[#86BBBD]"
              />
              <p className="py-1 ml-3 text-white text-center font-bold">Évaluer ce que tu regardes</p>
            </li>
            <li className="flex mb-4">
              <img
                src="/television.svg"
                alt="Partager ton avis avec les Whatshow Fans"
                className="w-6 h-6 fill-[#86BBBD]"
              />
              <p className="py-1 ml-3 text-white font-bold">Partager ton avis avec les Whatshow Fans</p>
            </li>
          </ul>
          <div className="flex justify-center mt-6 mb-6">
            <Link href={"/login/#inscription"}>
              <Button className="transition duration-500 bg-[#FCDC4D] hover:bg-red-500 text-white font-bold px-4 rounded focus:outline-none focus:shadow-outline">
                Inscription
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}