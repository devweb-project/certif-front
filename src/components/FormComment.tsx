import { Reviews } from '@/entities/Reviews';
import { Button, Label, Textarea } from 'flowbite-react';
import { FormEvent, useState } from 'react';

interface Props {
  onSubmit: (review: Reviews) => void;
};

export default function FormComment({ onSubmit }: Props) {
  const [errors, setErrors] = useState('');
  const [comment, setComment] = useState('');
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState<number | null>(null);

  function handleCommentChange(event: any) {
    setComment(event.target.value);
  };

  function handleRatingChange(currentRating: number) {
    setRating(currentRating);
  };

  async function handleSubmit(event: FormEvent) {
    event.preventDefault();

    if (rating === 0) {
      setErrors("Veuillez attribuer une note avant d'enregistrer.");
      return;
    }

    try {
      const review: Reviews = {
        comment,
        rating,
      };
      console.log("Review submitted:", review);
      onSubmit(review);
    } catch (error: any) {
      if (error.response?.status === 400) {
        setErrors(error.response.data.detail);
      }
    }
  }

  return (
    <form
      className="max-w-lg mx-auto border border-white rounded-lg p-4"
      id="textarea"
      onSubmit={handleSubmit}
    >
      <div className="mb-2 block">
        <Label
          className='text-xl font-bold'
          htmlFor="rating"
          value="Noter en PopCorn!!"
        />
      </div>
      {/* Gestion de la note */}
      <div className="flex justify-center text-center my-4">
        {[...Array(5)].map((_, index) => {
          const currentRating = index + 1;
          const isActive = currentRating <= rating;
          const isHovered = currentRating <= (hover || rating);

          return (
            <div key={index} className="flex items-center">
              <label>
                <input
                  type="radio"
                  name="rating"
                  className="hidden"
                  onClick={() => handleRatingChange(currentRating)}
                  onMouseEnter={() => setHover(currentRating)}
                  onMouseLeave={() => setHover(null)}
                />
                <svg
    fill="currentColor"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 792 792"
    className={`transition-transform duration-300 ${
      isHovered ? "scale-110 text-[#FCDC4D]" : "scale-100 text-gray-700"
    }`}
    width="40"
    height="40"
  >
    <path d="M669.584,248.165c0.887-5.423,1.355-10.96,1.355-16.574c0-38.039-21.123-72.163-53.447-89.497 c-8.236-49.393-51.28-87.178-102.979-87.178c-4.612,0-9.199,0.304-13.723,0.899c-18.373-16.193-42.31-25.469-67.221-25.469 c-1.749,0-3.497,0.051-5.246,0.139C404.769,10.72,375.371,0,343.972,0c-54.018,0-101.509,32.134-121.77,81.159 c-16.333,11.531-28.586,27.864-35.125,46.63c-39.661,18.5-66.03,58.807-66.03,103.802c0,5.588,0.405,11.125,1.204,16.574h-18.614 L212.369,792h367.262l108.731-543.835H669.584z M607.254,434.191H490.527c11.023,24.797,17.423,54.22,17.423,85.796 c0,31.108-6.209,60.125-16.941,84.694h83.883l-15.61,78.055H232.719l-15.599-78.055h83.883 c-10.732-24.569-16.941-53.599-16.941-84.694c0-31.563,6.399-60.986,17.423-85.796h-116.74l-15.611-78.055h453.717 L607.254,434.191z" />
    <path d="M422.179,137.685c-2.116,1.863-3.853,3.992-5.233,6.272c9.884-4.384,21.604-2.344,28.865,5.892 c7.248,8.224,7.806,20.122,2.218,29.372c2.433-1.077,4.764-2.534,6.88-4.397c10.694-9.427,12.051-25.393,3.003-35.644 C448.877,128.917,432.873,128.258,422.179,137.685z" />
  </svg>
              </label>
            </div>
          );
        })}
      </div>

      {/* Gestion des erreurs */}
      {errors && (
        <div className="text-red-500 text-sm mt-2">{errors}</div>
      )}
      <div className="mb-2 block">
        <Label
          className='text-xl font-bold'
          htmlFor="comment"
          value="Commenter ici"
        />
      </div>
      <Textarea
        id="comment"
        name='comment'
        className='w-80'
        placeholder="Entrer votre message..."
        value={comment}
        onChange={handleCommentChange}
        required
        rows={6}
      />



      <div className="flex justify-center m-2">
        <Button className="RecordButton" type="submit">
          Enregistrer
        </Button>
      </div>
    </form>
  )
};