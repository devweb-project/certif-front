import { AuthContext } from "@/auth/auth-context";
import Link from "next/link";
import { useContext, useState } from "react";
import jwtDecode from "jwt-decode";
import { useRouter } from "next/router";


export default function NavBar() {
    const { token, setToken } = useContext(AuthContext);
    const connectedColor = "#FBBF24";
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const router = useRouter();
    
    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    function handleLogout() {
        setToken(null);
        if (setToken == null) {
            router.push('/')
        }
    }

    function isAdmin() {
        if (token) {
            const decoded = jwtDecode<any>(token);
            if (decoded.roles[0] == "ROLE_ADMIN") {
                return true;
            }
            return false
        }
    }

    return (
        <>
            <header>
                <nav className="bg-white border-gray-200 px-4 lg:px-6 py-2.5 dark:bg-gray-800">
                    <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
                        <Link href={"/"} className="flex items-center">
                            <img
                                src="/television.svg"
                                alt="Television Icon"
                                className="mr-2"
                                width="36"
                                height="36"
                            />
                            <p className="mt-3 text-[#0E3B43] text-3xl font-semibold">WhatshoW</p>
                        </Link>

                        <div className="flex items-center lg:order-2">
                            <div className="nav-link relative" onClick={toggleMenu} >
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill={token ? connectedColor : "currentColor"} className="bi bi-person-workspace text-[#0E3B43] hover:text-[#86BBBD] inline-flex items-center" viewBox="0 0 16 16">
                                    <path d="M4 16s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H4Zm4-5.95a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z" />
                                    <path d="M2 1a2 2 0 0 0-2 2v9.5A1.5 1.5 0 0 0 1.5 14h.653a5.373 5.373 0 0 1 1.066-2H1V3a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v9h-2.219c.554.654.89 1.373 1.066 2h.653a1.5 1.5 0 0 0 1.5-1.5V3a2 2 0 0 0-2-2H2Z" />
                                </svg>
                            </div>

                        </div>

                        {isMenuOpen && (
                            <div className="w-full lg:w-auto">
                                <ul className="flex flex-col lg:flex-row lg:justify-center mt-4 px-4 lg:mt-0 font-medium">
                                    {token ? (
                                        <li className="lg:items-center lg:px-4">
                                            <Link href={'/'}>
                                                <button className="block py-2 text-[#0E3B43] rounded hover:text-[#86BBBD]" onClick={handleLogout}>Se déconnecter</button>
                                            </Link>
                                        </li>
                                    ) : (
                                        <li className="lg:items-center">
                                            <Link href={"/login"}>
                                                <button className="block py-2 text-[#0E3B43] rounded hover:text-[#86BBBD]">Se connecter/S'inscrire</button>
                                            </Link>
                                        </li>
                                    )}
                                    {token && (
                                        <li className="lg:items-center">
                                            <Link href={"/account"}>
                                                <button className="block py-2 text-[#0E3B43] rounded hover:text-[#86BBBD]">Mon Espace</button>
                                            </Link>
                                        </li>
                                    )}
                                </ul>
                            </div>
                        )}
                    </div>
                </nav>
            </header>
        </>
    )
}