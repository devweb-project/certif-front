

export interface Reviews {
    id?: number;
    comment?: string;
    rating?: number;
    idShow?: number;
    idUser?: number;
    userEmail?: string;
}