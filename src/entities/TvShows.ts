import { Reviews } from "./Reviews";

export interface TvShows {
    id?: number;
    image?: string;
    name?: string;
    genres?: string;
    status?: string;
    description?: string;
    link?: string;
    reviews?:Reviews[];
    showId: string;
    averageRating?: number;
} 


