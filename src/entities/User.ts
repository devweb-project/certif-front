import { Reviews } from "./Reviews";
import { TvShows } from "./TvShows";

export interface User {
    id?: number,
    email: string,
    password: string,
    role?: string;
    shows?: TvShows[],
    showReviews?: Reviews[]
}
