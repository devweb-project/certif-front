import { setCookie, destroyCookie, parseCookies } from "nookies";
import { createContext, useEffect, useState } from "react";

interface AuthState {
    token?: string | null;
    setToken: (value: string | null) => void
}

export const AuthContext = createContext({} as AuthState);

export const AuthContextProvider = ({ children }: any) => {
    const [token, setToken] = useState<string | null>('');

    function handleSet(value: string | null) {
        if (value) {
            setCookie(null, 'token', value)
        } else {
            destroyCookie(null, 'token');
        }
        setToken(value);
    }

    useEffect(() => {
        setToken(parseCookies().token);

    }, []);

    return (
        <AuthContext.Provider value={{ token, setToken: handleSet }}>
            {children}
        </AuthContext.Provider>
    );

}

