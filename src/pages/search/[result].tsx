import { TvShows } from "@/entities/TvShows";
import { fetchSearch } from "@/services/tvShow-service";
import { GetServerSideProps } from "next";
import CardShow from "@/components/CardShow";
import { useRouter } from "next/router";
import Link from "next/link";

interface Props {
    shows: TvShows[],
}

export default function ResultPage({ shows }: Props) {
    const router = useRouter();
    const showList = shows.length > 0 ? "showList card-container container-fluid" : "flex justify-content-center";

    return (
        <>
            <div className="bg-[#0E3B43] border-[#86BBBD] text-white font-bold text-xl p-2">
                <div className="flex justify-center items-center">
                    <Link href={"/"} className="my-8 text-[#86BBBD] text-center font-bold hover:text-[#FCDC4D] transition">
                        <img
                            src="/home.svg"
                            alt="Home Icon"
                            className="inline-block align-bottom"
                            width="28"
                            height="28"
                        />
                        Retour à l'Accueil
                        {/* <i className="fas fa-chevron-circle-left fa-fw"></i> */}
                    </Link>
                </div>
                <p>Résultat des recherches</p>
                <div className={showList}>

                    {Array.isArray(shows) && shows.length > 0 ? (
                        shows.map((item) => (
                            <CardShow key={item.id} tvShow={item} />
                        ))) : (
                        <p>Désolé, pas de résultat.</p>

                    )}

                </div>
            </div>
        </>
    );

}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    const { result } = context.query;

    return {
        props: {
            shows: await fetchSearch(String(result)),
        },
    };
};
