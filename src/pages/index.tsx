import Benefits from "@/components/Benefits";
import CardShow from "@/components/CardShow";
import SearchBar from "@/components/SearchBar";
import { TvShows } from "@/entities/TvShows";
import { fetchAlltvShows } from "@/services/tvShow-service";
import 'flowbite';
import { GetServerSideProps } from "next";

interface Props {
  tvShows: TvShows[];
}


export default function Home({ tvShows }: Props) {
  return (
    <>
      <SearchBar />
      <Benefits />
      <div className="bg-[#0E3B43] border-[#86BBBD] text-white font-bold text-2xl px-6">
        <p>Le top de la semaine</p>
      </div>
      <div className="showList whitespace-nowrap bg-[#0E3B43]">
        <div className="flex space-x-2">
          {tvShows.map((item) => <CardShow key={item.id} tvShow={item} />)}
        </div>
      </div>
    </>
  )
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
  try {
    return {
      props: {
        tvShows: await fetchAlltvShows()
      }
    };
  } catch {
    return {
      notFound: true
    }
  }
};