
import { AuthContext } from "@/auth/auth-context";
import { addShowsToWatchList, fetchUser } from "@/auth/auth-service";
import { Reviews } from "@/entities/Reviews";
import { TvShows } from "@/entities/TvShows";
import { User } from "@/entities/User";
import { deleteReview } from "@/services/review-service";
import { fetchOnetvShow } from "@/services/tvShow-service";
import { AxiosError } from "axios";
import jwtDecode from "jwt-decode";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";

export default function ShowPage() {
    const router = useRouter();
    const { id } = router.query;
    const { token, setToken } = useContext(AuthContext);
    const [show, setShow] = useState<TvShows>();
    const [user, setUser] = useState<User>();

    const handleAddToWatchList = async () => {
        console.log("Tentative d'ajout à la liste de suivi");
        try {
            await addShowsToWatchList(Number(id));
            console.log("Ajout réussi");
        } catch (error) {
            if (error instanceof AxiosError) {
                if (error.response && error.response.status === 400) {
                    alert("Déjà dans ma watchList"); // Affichez un message d'alerte
                } else {
                    console.error("Erreur lors de l'ajout à la liste de suivi", error);
                    return {
                        notFound: true
                    };
                }
            } else {
                console.error("Erreur inconnue lors de l'ajout à la liste de suivi", error);
            }
        }
    };

    function isAdmin() {
        if (token) {
            const decoded = jwtDecode<any>(token);
            if (decoded.roles[0] == "ROLE_ADMIN") {
                return true;
            }
            return false;
        }

    }

    useEffect(() => {
        if (id) {
            fetchOnetvShow(Number(id))
                .then(data => setShow(data))
                .catch(error => {
                    if (error.response.status == 404) {
                        router.push('/404');
                    }
                });
        }
    }, [id]);

    useEffect(() => {
        if (token) {
            fetchUser()
                .then(data => {
                    setUser(data);
                })
                .catch(error => {
                    if (error.response.status === 401) {
                        router.push("/login");
                    }
                });
        }
    }, [token]);


    const handleDeleteReview = async (reviewId?: number) => {
        if (reviewId !== undefined) {
            try {
                await deleteReview(reviewId);
                fetchOnetvShow(Number(id))
                    .then(data => setShow(data))
                    .catch(error => {
                        if (error.response.status == 404) {
                            router.push('/404');
                        }
                    });
            } catch (error) {
                console.log(error);
                return {
                    notFound: true
                }
            }
        }
    };

    return (
        <>
            <div className="container mx-auto">
                <div className="bg-white px-28 border-top-1">
                    <div className="title text-center flex flex-col items-center">
                        <div className="flex justify-center items-center">
                            <Link href={"/"} className="my-8 text-[#86BBBD] text-center font-bold hover:text-[#FCDC4D] transition">
                                <img
                                    src="/home.svg"
                                    alt="Home Icon"
                                    className="inline-block align-bottom"
                                    width="28"
                                    height="28"
                                />
                                Retour à l'Accueil
                            </Link>
                        </div>
                        <h5 className="mb-2 mt-2 text-3xl font-bold text-[#0E3B43]">{show?.name}</h5>
                        {show?.link && (
                            <div className="flex justify-center items-center p-2">
                                <a
                                    target='_blank'
                                    rel="noopener noreferrer"
                                    href={show.link}
                                    className="flex items-center font-bold text-[#0E3B43] px-1 hover:text-[#86BBBD] transition-colors duration-200"
                                >
                                    <img
                                        src="/link-icon.svg"
                                        alt="Link Icon"
                                        className="mr-2"
                                        width="32"
                                        height="32"
                                    />
                                    <span>Regarder</span>
                                </a>
                            </div>
                        )}
                    </div>

                    <div className="p-5 flex flex-col lg:flex-row items-center justify-between">

                        <img className="rounded-lg card-img-top imageShow mb-4 sm:mb-0 sm:mr-10" src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/${show?.image}`}
                            alt="tvShow image" />
                        <div className="options text-center text-2xl">
                            {show?.reviews && show.reviews.length > 0 ? (
                                <p className="px-3 mb-3 font-bold">Note à Jour: <span className="text-[#0E3B43]">{show.averageRating?.toFixed(0)}/5</span></p>
                            ) : (
                                <p className="mb-1 font-normal text-[#0E3B43]">Aucune note dispo.</p>
                            )}
                            <div className="flex-col">
                                <a
                                    className="px-3 mb-3 font-bold cursor-pointer"
                                    onClick={async () => {
                                        await handleAddToWatchList();
                                        router.push('/account');
                                    }}
                                >
                                    Ajouter
                                    <p className="px-3 mb-3 font-bold flex justify-center">
                                        <img
                                            src="/add-icon.svg"
                                            alt="Add Icon"
                                            className="plus"
                                            width="32"
                                            height="32"
                                        />
                                    </p>
                                </a>
                            </div>
                            <div className="flex-col">
                                {token ? (
                                    <Link href={`/review/${id}`}>
                                        <span className="px-3 mb-3 font-bold">Evaluer</span>
                                        <p className="px-3 mb-3 font-bold flex justify-center">
                                            <img
                                                src="/review-icon.svg"
                                                alt="Review Icon"
                                                className="popCorn"
                                                width="32"
                                                height="32"
                                            />
                                        </p>
                                    </Link>
                                ) : (
                                    <p className="px-3 mb-3 font-bold flex justify-center]">
                                        Connectez-vous pour laisser un avis.
                                    </p>
                                )}
                            </div>
                        </div>
                        <div className="p-2 md:ml-10 border border-[#0E3B43] rounded-3xl">
                            <h2 className="mb-3 font-bold text-[#0E3B43]">Synopsis</h2>
                            <p className="mb-3 font-normal text-[#0E3B43]">{show?.description}</p>
                            <p className="mb-1 text-1xl tracking-tight text-[#0E3B43]">Genre: {show?.genres}</p>
                            <h3 className="mb-2 font-bold tracking-tight text-[#0E3B43]">Statut: {show?.status}</h3>
                        </div>
                    </div>
                    
                    <div className="comments-section p-5">
                        <h2 className="mb-3 font-bold text-[#0E3B43]">Commentaires</h2>
                        {show?.reviews && show.reviews.length > 0 ? (
                            show.reviews.map(review => (
                                <div key={review.id}
                                    className="p-2 mb-3 border border-[#243c5a] rounded-3xl"
                                    style={{ width: 'fit-content' }}
                                >
                                    <h3>{review.userEmail}</h3>
                                    <p className="mb-1 font-normal text-[#0E3B43]">{review.comment}</p>
                                    {/* <p className="mb-1">Note: {review.rating}/5</p> */}
                                    {isAdmin() &&
                                        <button className="delete flex font-bold items-center px-1" onClick={() => handleDeleteReview(review.id)}>
                                            Effacer le commentaire
                                        </button>
                                    }
                                </div>
                            ))
                        ) : (
                            <p className="mb-1 font-normal text-[#0E3B43]">Aucun commentaire disponible.</p>
                        )}
                    </div>
                </div>
            </div>
        </>
    )
}