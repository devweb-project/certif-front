import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import "@/auth/axios-config"
import NavBar from '@/components/NavBar';
import SearchBar from '@/components/SearchBar';
import Footer from '@/components/Footer';
import { AuthContextProvider } from '@/auth/auth-context';
import Head from 'next/head'; 

export default function App({ Component, pageProps }: AppProps) {

  return (
    <>
    <Head> <title>Whatshow</title>
    <link rel= "shortcut icon" href="television.svg"></link></Head>
      <AuthContextProvider>
        <NavBar />
        <main>
          <Component {...pageProps} />
        </main>
        <Footer />
      </AuthContextProvider>
    </>)
}
