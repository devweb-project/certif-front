import LoginForm from "@/components/LoginForm";
import SignUpForm from "@/components/SignUpForm";


export default function loginPage() {

    return (
        <>
            <div className="bg-[#0E3B43]">
                <LoginForm from="login" />
                <SignUpForm />
            </div>
        </>
    )
}