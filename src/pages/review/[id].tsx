import { Reviews } from "@/entities/Reviews";
import { TvShows } from "@/entities/TvShows";
import { postReview } from "@/services/review-service";
import { fetchOnetvShow } from "@/services/tvShow-service";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Link from "next/link";
import FormComment from "@/components/FormComment";


export default function ReviewPage() {
    const router = useRouter();
    const { id } = router.query;
    const [show, setShow] = useState<TvShows>();
    async function add(review: Reviews) {
        try {
            if (show && show.id) {
                await postReview(review, show.id);
                router.push('/show/' + show.id)
            }
        } catch (error) {
            console.log(error);
            return {
                notFound: true
            }
        }
    }

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOnetvShow(Number(id))
            .then(data => setShow(data))
            .catch(error => {
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });
    }, [id]);

    return (
        <div className="container-fluid">
            {show && (
                <div className="px-28 border-top-1">
                    <div className="bg-[#0E3B43]">
                        <div className="text-center items-center flex-row">
                            <h5 className="text-3xl items-center text-white font-bold">{show.name}</h5>
                        </div>
                        <div className="flex justify-center items-center">
                            <Link href={"/"} className="my-8 text-[#86BBBD] text-center font-bold hover:text-[#FCDC4D] transition">
                                <img
                                    src="/home.svg"
                                    alt="Home Icon"
                                    className="inline-block align-bottom"
                                    width="28"
                                    height="28"
                                />
                                Retour à l'Accueil
                                <i className="fas fa-chevron-circle-left fa-fw"></i>
                            </Link>
                        </div>
                        <div className="flex flex-col sm:flex-row items-center justify-center">
                            <Link href={`/show/${id}`}>
                                <img className="rounded-lg card-img-top imageShow mb-4 sm:mb-0 sm:mr-10" src={`${process.env.NEXT_PUBLIC_API_BASE_URL}/${show?.image}`}
                                    alt="tvShow image" />
                            </Link>
                        </div>
                    </div>
                    <div className="w-full bg-[#86BBBD] py-4">
                        <div className="text-center text-xl p-3">
                        <p className="font-bold">Note Moyenne: <span className="text-[#0E3B43]">
                            {show.averageRating}/5</span></p>
                        </div>
                        <div className="flex justify-center">
                            <div className="text-center border-b">
                                <div className="flex justify-center py-3">
                                    <FormComment onSubmit={add} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}
